# Forking Demo

This project is a demo to show students how to do a forking workflow. 

The project consists of a simple readme with a list of contributors.

Students fork the repo, add their name to the contributors and submit a Merge Request.

## Instructions to Contribute

1. Fork this repo.
2. Clone your fork.
3. Edit the Readme.md file to add your name to the list of contributors.
4. Commit and push your updated Readme to your fork.
5. Open a merge request to merge your changes into this repository.

## Keeping your fork up to date

1. Clone your fork, so you have a local repository
2. Create a remote named upstream that points to this repository: gitlab.com/sas-uge/forking-demo
3. Fetch upstream
4. Rebase onto upstream/main
5. Push to origin
 
 ## Contributors
 
 - Sam Taggart 
